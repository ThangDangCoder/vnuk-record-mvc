
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib tagdir="/WEB-INF/tags" prefix="vnuk"%>
<html>
	<head>
		<link href="css/jquery-ui.css" type="text/css" rel="stylesheet">
		<link href="bootstrap/bootstrap.min.css" type="text/css" rel="stylesheet">	
		<script src="js/jquery.js" type="text/javascript"></script>
		<script src="js/jquery-ui.js" type="text/javascript"></script>
	</head>
	
	<body>
		<c:import url="../header.jsp"/>
		<h1>Update contact</h1>
		<hr/>
		<form action="updateContact" method="POST">
			<input type="hidden" name="id" value="${contact.id}"/>
			Name:<input type="text" name="name" value="${contact.name}"/><br/>
			E-mail:<input type="text" name="email" value="${contact.email}"/><br/>
			Address:<input type="text" name="address" value="${contact.address}"/><br/>
			Date of birth:<input type="text" id="date_of_birth" name="date_of_birth" 
							value=<fmt:formatDate value="${contact.dateOfBirth.time}" pattern="dd/MM/yyyy"/> /><br/>
			<input type="submit" class="btn btn-info" value="Save"/>
		</form>
		<c:import url="../footer.jsp"></c:import>
	</body>


</html>