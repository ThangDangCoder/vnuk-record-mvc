
<html>
	<head>
		<link href="bootstrap/bootstrap.min.css" type="text/css" rel="stylesheet">	
		<link href="font-awesome/css/font-awesome.min.css" type="text/css" rel="stylesheet">
	</head>
	<body>
		<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
		<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"  %>
		
		<c:import url="../header.jsp"></c:import>
		
		<a href="mvc?logic=contact.Create">
			<button type="button" class="btn btn-primary" title="Add new contact">
				<i class="fa fa-plus" aria-hidden="true"></i>
				Add a new contact
			</button>
		</a>
		
		<table>
			<thead>
				<tr>
					<th>Name</th>
					<th>Email</th>
					<th>Address</th>
					<th>Day of birth</th>
					<th>Action</th>
				</tr>
			</thead>
			
			<tbody>
				<c:forEach var="contact" items="${contacts}">
				
					<tr>
						<td>${contact.name}</td>
						<td>
							<c:choose>
								<c:when test = "${not empty contact.email}">
									<a href="mailto:${contact.email}">${contact.email}</a>
								</c:when>
								
								<c:otherwise>
									<i>No email address</i>
								</c:otherwise>	
							</c:choose>
						</td>
						<td>${contact.address}</td>
						<td>
							<fmt:formatDate value="${contact.dateOfBirth.time}" pattern="dd/MM/yyyy"/></td>			
						<td>
							<a href="mvc?logic=contact.Delete&id=${contact.id}">
								<button type="button" class="btn btn-danger" title="Delete this contact">
									<i class="fa fa-times" aria-hidden="true"></i>
									Delete
								</button>
							</a>
						</td>
						
						<td>
							<a href="mvc?logic=contact.Show&id=${contact.id}">
								<button type="button" class="btn btn-success" title="Show this contact">
									<i class="fa fa-eye" aria-hidden="true"></i>
									Show
								</button>
							</a>
						</td>
					</tr>
					
				</c:forEach>
			</tbody>
			
		</table>
		<c:import url="../footer.jsp"></c:import>
	</body>
</html>