
<html>
	<head>
		<link href="bootstrap/bootstrap.min.css" type="text/css" rel="stylesheet">
	</head>
	<body>
		<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
		<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"  %>
	
		Id : ${contact.id}<br/>
	    Name : ${contact.name}<br/>
	    E-mail : ${contact.email}<br/>
	    Address : ${contact.address}<br/>
	    Date of birth : <fmt:formatDate value="${contact.dateOfBirth.time}" pattern="dd/MM/yyyy" /><br/>
	    
	    <br/>
	    <a href="mvc?logic=contact.Index">
			<button type="button" class="btn" title="Get back">Back</button>
		</a>
		
	    <a href="mvc?logic=contact.Update&id=${contact.id}">
	    	<button type="button" class="btn btn-primary" title="Update this contact">Update</button>
	    </a>
	    
	</body>
</html>