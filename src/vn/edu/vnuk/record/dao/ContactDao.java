package vn.edu.vnuk.record.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import vn.edu.vnuk.record.model.Contact;

public class ContactDao {

	private Connection connection;
//
//	public ContactDao() {
//		this.connection = new ConnectionFactory().getConnection();
//	}
	
	public ContactDao(Connection connection) {
		this.connection = connection;
	}
	
	public void create(Contact contact) throws SQLException {
		
		String sqlQuery = "insert into contacts (name, email, address, date_of_birth ) "
				+ "values (?, ?, ?, ?)";
		PreparedStatement statement;
		
		try {
			statement = connection.prepareStatement(sqlQuery);
			statement.setString(1, contact.getName());
			statement.setString(2, contact.getEmail());
			statement.setString(3, contact.getAddress());
			statement.setDate(4, new java.sql.Date(contact.getDateOfBirth().getTimeInMillis()));
			
			statement.execute();
			statement.close();
			System.out.println("New Record in DB!");
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			System.out.println("Done!");
		}
	}
	
	@SuppressWarnings("finally")
	public List<Contact> read() throws SQLException {
		
		String sqlQuery = "select * from contacts";
		PreparedStatement statement;
		List<Contact> contacts = new ArrayList<Contact>();
		
		try {
			statement = connection.prepareStatement(sqlQuery);
			ResultSet results = statement.executeQuery();
			while(results.next()) {
				contacts.add(buildContact(results));
			}
			
			statement.execute();
			results.close();
			statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			return contacts;
		}
	}
	
	@SuppressWarnings("finally")
	public Contact read(long id) throws SQLException {
		Contact contact = new Contact();
		
		String sqlQuery = "select * from contacts where id = '" + id + "'";
		PreparedStatement statement;
		
		try {
			statement = connection.prepareStatement(sqlQuery);
			ResultSet results = statement.executeQuery();
			if(results.next()) {
				contact = buildContact(results);
			}
			
			statement.execute();
			results.close();
			statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			return contact;
		}
	}
	
	public void update(Contact contact) throws SQLException {
		String sqlQuery = "UPDATE contacts SET name=?, email=?, address=?, date_of_birth=? WHERE id=?";
		PreparedStatement statement;
		
		try {
			statement = connection.prepareStatement(sqlQuery);
			statement.setString(1, contact.getName());
			statement.setString(2, contact.getEmail());
			statement.setString(3, contact.getAddress());
			statement.setDate(4, new java.sql.Date(contact.getDateOfBirth().getTimeInMillis()));
			statement.setLong(5, contact.getId());
			statement.execute();
			statement.close();
			System.out.println("Successfully updated!");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void destroy(long id) throws SQLException {
		String sqlQuery = "DELETE FROM contacts WHERE id = '" + id + "'" ;
		PreparedStatement statement;
		
		try {
			//if(existContact(id)) {
				statement = connection.prepareStatement(sqlQuery);
				
				statement.execute();
				statement.close();
				System.out.println("Successfully destroyed!");
			//}
			//else
			//	System.out.println("Record not existing...");
		} catch (SQLException e) {
			e.printStackTrace();
		} 
		
	}
	
	public boolean existContact(long id) throws SQLException {
		String sqlQuery = "SELECT 1 FROM contacts WHERE id = ? ";
		PreparedStatement statement;
		
		try {
			
			statement = connection.prepareStatement(sqlQuery);
			statement.setLong(1, id);
			ResultSet results = statement.executeQuery();
			
			if(results.next()) {
				statement.execute();
				results.close();
				statement.close();
				return true;
			}
			else {
				
				return false;
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} 
		return true;
	}
	
	private Contact buildContact(ResultSet results) throws SQLException {
		Contact contact = new Contact();
		contact.setId(results.getLong("id"));
		contact.setName(results.getString("name"));
		contact.setEmail(results.getString("email"));
		contact.setAddress(results.getString("address"));
		
		Calendar date = Calendar.getInstance();
		date.setTime(results.getDate("date_of_birth"));
		contact.setDateOfBirth(date);
		
		return contact;
	}
}
	