package vn.edu.vnuk.record.mvc.servlet.contact;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import vn.edu.vnuk.record.dao.ContactDao;
import vn.edu.vnuk.record.model.Contact;

@WebServlet("/addContact")
@SuppressWarnings("serial")
public class Add extends HttpServlet{
	
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		log("Starting servlet");
	}
	
	@Override
	public void destroy() {
		super.destroy();
		log("Ending servlet");
	}
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Connection connection = (Connection) request.getAttribute("myConnection");
		PrintWriter out = response.getWriter();
		String name = request.getParameter("name");
		String email = request.getParameter("email");
		String address = request.getParameter("address");
		String dateAsString = request.getParameter("date_of_birth");
		
		Calendar dateOfBirth = null;
		try {
			Date date = new SimpleDateFormat("dd/MM/yyyy").parse(dateAsString);
			dateOfBirth = Calendar.getInstance();
			dateOfBirth.setTime(date);
		} catch (ParseException e) {
			out.println("Error while converting day of birth...!");
			return;
		}
		
		Contact contact = new Contact();
		contact.setName(name);
		contact.setEmail(email);
		contact.setAddress(address);
		contact.setDateOfBirth(dateOfBirth);
		
		ContactDao contactDao = new ContactDao(connection);
		try {
			contactDao.create(contact);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		RequestDispatcher rd = request.getRequestDispatcher("/mvc?logic=contact.Index");
		rd.forward(request,response);
	}
}
