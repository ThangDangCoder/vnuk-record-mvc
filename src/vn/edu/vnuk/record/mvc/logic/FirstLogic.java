package vn.edu.vnuk.record.mvc.logic;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class FirstLogic implements Logic{

	@Override
	public String run(HttpServletRequest request, HttpServletResponse response) throws Exception{
		System.out.println("Running login and redirecting...");
		return "first-logic.jsp";
	}
	
}
