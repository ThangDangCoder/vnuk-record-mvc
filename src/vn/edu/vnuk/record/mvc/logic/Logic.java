package vn.edu.vnuk.record.mvc.logic;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface Logic {
	String run(HttpServletRequest request, HttpServletResponse response) throws Exception;
}
