package vn.edu.vnuk.record.mvc.logic.contact;

import java.sql.Connection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import vn.edu.vnuk.record.dao.ContactDao;
import vn.edu.vnuk.record.mvc.logic.Logic;

public class Delete implements Logic {

	@Override
	public String run(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Connection connection = (Connection) request.getAttribute("myConnection");
		long id = Long.parseLong(request.getParameter("id"));
		new ContactDao(connection).destroy(id);
		System.out.println("Deleting contact...");
		return "mvc?logic=contact.Index";
	}

}
