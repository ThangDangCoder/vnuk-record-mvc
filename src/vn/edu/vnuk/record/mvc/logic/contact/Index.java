package vn.edu.vnuk.record.mvc.logic.contact;

import java.sql.Connection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import vn.edu.vnuk.record.dao.ContactDao;
import vn.edu.vnuk.record.mvc.logic.Logic;

public class Index implements Logic{

	@Override
	public String run(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Connection connection = (Connection) request.getAttribute("myConnection");
		request.setAttribute("contacts", new ContactDao(connection).read());
		return "/WEB-INF/jsp/contact/index.jsp";
	
	}
	
}
